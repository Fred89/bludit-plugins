[Bludit Plugins](https://plugins.bludit.com)
================================
**---> This repository is no longer maintained. <---**

These plugins are only compatible with Bludit v1.x., NOT for Bludit v2.x.

The new repository for Bludit plugins
- https://github.com/bludit/plugins-repository

Official page of plugins
- https://plugins.bludit.com
